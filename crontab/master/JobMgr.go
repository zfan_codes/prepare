package master

import (
	"context"
	"encoding/json"
	"go.etcd.io/etcd/clientv3"
	"prepare/crontab/common"
	"time"
)

// 任务管理器
type JobMgr struct {
	client *clientv3.Client
	kv     clientv3.KV
	lease  clientv3.Lease
}

var (
	G_jobMgr *JobMgr
)

func InitJobMgr() (err error) {
	var (
		config clientv3.Config
		client *clientv3.Client
		lease  clientv3.Lease
		kv     clientv3.KV
	)

	// 初始化配置
	config = clientv3.Config{
		Endpoints:   G_config.EtcdEndpoints,
		DialTimeout: time.Duration(G_config.EtcdDialTimeout) * time.Millisecond,
	}

	// 建立连接
	if client, err = clientv3.New(config); err != nil {
		return
	}

	// 获取kv操作器
	kv = clientv3.NewKV(client)
	lease = clientv3.NewLease(client)

	// 赋值单例
	G_jobMgr = &JobMgr{
		client: client,
		kv:     kv,
		lease:  lease,
	}
	return
}

func (jobMgr *JobMgr) SaveJob(job *common.Job) (oldJob *common.Job, err error) {
	// 把任务保存到/cron/jobs/任务名-> json
	var (
		jobKey    string
		jobValue  []byte
		putResp   *clientv3.PutResponse
		objJobObj common.Job
	)
	jobKey = "/cron/jobs"

	// etcd的保存信息
	jobKey = "/cron/jobs/" + job.Name
	// 任务信息json
	if jobValue, err = json.Marshal(job); err != nil {
		return
	}

	// 保存到etcd / 并且获取旧的任务
	if putResp, err = jobMgr.kv.Put(context.TODO(), jobKey,
		string(jobValue), clientv3.WithPrevKV()); err != nil {
		return
	}

	// 如果是更新，返回旧值
	if putResp.PrevKv != nil {
		// 对旧值反序列化
		if err = json.Unmarshal(putResp.PrevKv.Value, &objJobObj); err != nil {
			err = nil
			return
		}
		oldJob = &objJobObj
	}

	return
}

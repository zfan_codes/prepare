package main

import (
	"flag"
	"fmt"
	"prepare/crontab/master"
	"runtime"
	"time"
)

var (
	confFile string // 配置文件路径
)

// 初始化线程
func initEnv() {
	// 设置线程数量和cpu数量相等， 可以发挥最大的效率
	runtime.GOMAXPROCS(runtime.NumCPU())
}

// 解析命令行参数
func initArgs() {
	// master -config ./master.json
	flag.StringVar(&confFile, "config", "./master.json", "指定master.json")
	flag.Parse() // 自动解析命令行参数
}

func main() {
	var (
		err error
	)
	// 初始化命令行参数
	initArgs()

	// 初始化线程
	initEnv()

	// 加载配置
	if err = master.InitConfig(confFile); err != nil {
		goto ERR
	}

	// 任务管理器
	if err = master.InitJobMgr(); err != nil {
		goto ERR
	}

	// 启动API http服务
	if err = master.InitApiServer(); err != nil {
		goto ERR
	}

	for {
		time.Sleep(1 * time.Second)
	}

	// 正常退出
	return
ERR:
	fmt.Println(err)
}

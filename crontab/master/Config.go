package master

import (
	"encoding/json"
	"io/ioutil"
)

type Config struct {
	ApiPort         int      `json:"api_port"`
	ApiReadTimeout  int      `json:"api_read_timeout"`
	ApiWriteTimeout int      `json:"api_write_timeout"`
	EtcdEndpoints   []string `json:"etcd_endpoints"`
	EtcdDialTimeout int      `json:"etcd_dial_timeout"`
}

var (
	G_config Config
)

// 加载配置
func InitConfig(filename string) (err error) {
	var (
		content []byte
		config  Config
	)

	// 将配置文件读进来
	if content, err = ioutil.ReadFile(filename); err != nil {
		return
	}

	// 做json反序列化
	if err = json.Unmarshal(content, &config); err != nil {
		return
	}

	// 赋值单例
	G_config = config
	return
}

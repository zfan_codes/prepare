package main

import (
	"fmt"
	"os/exec"
)

func main() {

	// 建议将所有变量定义在方法顶部，方便阅读
	var (
		cmd *exec.Cmd
		output []byte
		err error
	)

	// 生成cmd命令
	cmd = exec.Command("D:\\Program Files\\Git\\bin\\bash.exe","-c","sleep 5;ls -l;sleep 3;echo hello")

	// 建议将错误处理写成这种方式
	if output,err = cmd.CombinedOutput();err != nil{
		fmt.Println(err)
		return
	}

	// 打印子进程输出
	fmt.Println(string(output))
}

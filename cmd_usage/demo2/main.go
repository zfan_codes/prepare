package main

import (
	"context"
	"fmt"
	"os/exec"
	"time"
)

type result struct {
	err    error
	output []byte
}

func main() {
	// 执行一个cmd，让它在一个协程里去执行，让它执行2秒	sleep 2

	// 1秒的时候杀死cmd

	var (
		ctx        context.Context
		cancelFunc context.CancelFunc
		cmd        *exec.Cmd
		resultChan chan *result
		res *result
	)

	// 创建一个结果队列
	resultChan = make(chan *result, 1000)

	// 获取一个取消任务的上下文， 取消任务的方法是cancelFunc
	ctx, cancelFunc = context.WithCancel(context.TODO())

	go func() {
		var (
			output []byte
			err    error
		)
		cmd = exec.CommandContext(ctx, "D:\\Program Files\\Git\\bin\\bash.exe", "-c", "sleep 2;echo hello")

		// 执行任务，捕获输出
		output, err = cmd.CombinedOutput()
		// 将输出结果传给main协程
		resultChan <- &result{
			output: output,
			err:    err,
		}
	}()

	// 继续往下走
	time.Sleep(1 * time.Second)

	// 取消上下文
	cancelFunc()

	// 在main协程里等待子协程的退出，并打印任务执行结果
	res = <- resultChan

	fmt.Println(res.err,string(res.output))

}

package main

import (
	"context"
	"fmt"
	"github.com/mongodb/mongo-go-driver/bson"
	"github.com/mongodb/mongo-go-driver/mongo"
	"github.com/mongodb/mongo-go-driver/mongo/options"
	"time"
)

func main() {
	var (
		mongo_uri  string
		opts       *options.ClientOptions
		client     *mongo.Client
		err        error
		database   *mongo.Database
		collection *mongo.Collection
		delResult  *mongo.DeleteResult
	)
	// 1. 建立连接
	mongo_uri = "mongodb://127.0.0.1:27017"
	opts = options.Client()
	opts.SetConnectTimeout(5 * time.Second)
	if client, err = mongo.Connect(context.TODO(), mongo_uri, opts); err != nil {
		fmt.Println(err)
		return
	}
	// 2. 选择数据库 my_db
	database = client.Database("cron")
	// 3. 选择表 my_collection
	collection = database.Collection("logs")

	collection = collection
	// 删除 startTime 小于当前时间的所有数据
	find := bson.M{"timePoint.startTime": bson.M{"$lt": time.Now().Unix()}}
	if delResult, err = collection.DeleteMany(context.TODO(), find); err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("删除的行数", delResult.DeletedCount)
}

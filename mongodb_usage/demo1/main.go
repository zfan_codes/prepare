package main

import (
	"context"
	"fmt"
	"github.com/mongodb/mongo-go-driver/mongo"
	"github.com/mongodb/mongo-go-driver/mongo/options"
	"time"
)

func main() {
	var (
		mongo_uri  string
		opts       *options.ClientOptions
		client     *mongo.Client
		err        error
		database   *mongo.Database
		collection *mongo.Collection
	)
	// 1. 建立连接
	mongo_uri = "mongodb://127.0.0.1:27017"
	opts = options.Client()
	opts.SetConnectTimeout(5 * time.Second)
	if client, err = mongo.Connect(context.TODO(), mongo_uri, opts); err != nil {
		fmt.Println(err)
		return
	}
	// 2. 选择数据库 my_db
	database = client.Database("my_db")
	// 3. 选择表 my_collection
	collection = database.Collection("my_collection")

	collection = collection
}

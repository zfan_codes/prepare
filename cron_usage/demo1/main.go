package main

import (
	"fmt"
	"github.com/gorhill/cronexpr"
	"time"
)

func main() {
	var (
		expr *cronexpr.Expression
		err  error
		now time.Time
		nextTime time.Time
	)
	// linux
	// 哪一分钟（0-59），哪小时（0-23），哪天（1-31），哪月（1-12），星期几（0-6）
	// cronexpr
	// 秒 分 时 日 月 星期 年


	// 每5分钟执行一次
	if expr, err = cronexpr.Parse("*/5 * * * * * *"); err != nil {
		fmt.Println(err)
		return
	}

	// 获取当前时间
	now = time.Now()
	// 获取下一次执行时间
	nextTime = expr.Next(now)

	//fmt.Println(nextTime.Format("2006-01-02 15:04:05"))

	// nextTime.Sub(now) 计算两个时间的时间差
	time.AfterFunc(nextTime.Sub(now), func() {
		fmt.Println("被调度了:",nextTime.Format("2006-01-02 15:04:05"))
	})

	time.Sleep(5 * time.Second)
	fmt.Println("done")
}

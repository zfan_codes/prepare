package main

import (
	"fmt"
	"github.com/gorhill/cronexpr"
	"time"
)

// 代表一个任务
type CronJob struct {
	expr     *cronexpr.Expression
	nextTime time.Time // expr.Next(now)
}

func main() {
	// 需要有一个调度协程，它定时检查所有任务，谁过期了就执行谁
	var (
		cronJob       *CronJob
		expr          *cronexpr.Expression
		now           time.Time           // 当前时间
		scheduleTable map[string]*CronJob // 调度表
	)

	scheduleTable = make(map[string]*CronJob) //
	now = time.Now()

	// 定义两个cronjob
	// MustParse 和 Parse 的区别是我们默认前者没有错误返回
	expr = cronexpr.MustParse("*/5 * * * * * *")
	cronJob = &CronJob{
		expr:     expr,
		nextTime: expr.Next(now),
	}
	// 任务注册到调度表
	scheduleTable["job1"] = cronJob

	// ----> 重复注册一个job
	expr = cronexpr.MustParse("*/5 * * * * * *")

	cronJob = &CronJob{
		expr:     expr,
		nextTime: expr.Next(now),
	}
	// 任务注册到调度表
	scheduleTable["job2"] = cronJob

	// 启动一个调度协程
	go func() {
		var (
			jobName string
			cronJob *CronJob
			now     time.Time
		)
		// 定时检查一下任务调度表
		for {
			now = time.Now()

			for jobName, cronJob = range scheduleTable {
				// 当任务执行时间小于当前时间的时候执行这个任务
				if cronJob.nextTime.Before(now) || cronJob.nextTime.Equal(now) {
					go func(jobName string) {
						fmt.Println("调度", jobName)
					}(jobName)
					// 重新生成下次执行的时间

					cronJob.nextTime = cronJob.expr.Next(now)
					fmt.Println("下次执行时间：", cronJob.nextTime.Format("2006-01-02 15:04:05"))
				}
			}

			// 睡眠100毫秒
			select {
			case <-time.NewTimer(100 * time.Millisecond).C: // 将在100毫秒可读

			}
		}
	}()

	time.Sleep(100 * time.Second)
}

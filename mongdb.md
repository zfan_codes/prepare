# mongodb

### 数据库层操作

|命令|注释|
|:---|:---|
|`show databases`|列举数据库|
|`use my_db`|选择数据库|
|`show collections`|列举数据表|
|`db.createCollection("my_collection")`|创建表（db的前缀是固定的）|
|`db.my_collection.insertOne({uid:1001,name:"xiaoming",likes:["football","game"]})`|插入一条数据|
|`db.my_collection.find({likes:"football",name:{$in:["xiaoming","libai"]}}).sort({uid:1})`|查询一条数据，按照uid升序排序|
|`db.my_collection.updateMany({likes:"footbill"},{$set:{name:"李白"}})`|更新数据，第一个参数是过滤条件，第二个参数是更新操作|
|`db.my_collection.deleteMany({name:"李白"})`|删除条件|
|`db.my_collection.createIndex({uid:1,name:-1})`|创建索引，可以建立索引的正反顺序|

- 数据库无需创建，只是一个命名空间
- 数据是schema free,无需定义字段

### 和mysql聚合类比
|mysql|mongodb|注释|
|:--|:--|:--|
|`where`|`$match`||
|`group by`|`$group`||
|`having`|`$match`|对聚合数据进行第二次筛选|
|`select`|`$project`||
|`order by`|`$sort`||
|`limit`|`$limit`||
|`sum`|`$sum`||
|`count`|`$sum`||

### 聚合统计示例

```
db.my_collection.aggregate([
    {$unwind:"$likes"},
    {$group:{id:{likes:"$likes"}}},
    {$project:{_id:0,like:"$_id.likes",total:{$sum:1}}}
    ])
```
- aggregate 聚合方法，按照流水线进行处理->前一个阶段输出作为下一个阶段的输入
    1. {$unwind:"$likes"} 将 likes拆成独立的字段
    2. {$group:{id:{likes:"$likes"}}} 将第一步的字段进行聚合 喜欢足球的聚合到一起，喜欢游戏的聚合到一起
    3. 第三步进行统计
    4. 输出 {"like":"game","total":1} {"like":"football","total":1}


### mongodb 安装

```
mongod --dbpath=./data --bind_ip=0.0.0.0
```
- `--dbpath=./data` 指定下载目录
- `--bind_ip` 指定允许访问IP


win 下配置文件编写
mongo.config
```
dbpath=D:\Program Files\mongodb-win32-x86_64-2008plus-ssl-4.0.4\data
logpath=D:\Program Files\mongodb-win32-x86_64-2008plus-ssl-4.0.4\log\mongo.log
```
启动（管理员）
```
cd .\mongodb-win32-x86_64-2008plus-ssl-4.0.4\
.\bin\mongod.exe --config ".\mongo.config"
```
客户端
```
mongo.exe
```
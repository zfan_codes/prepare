package main

import (
	"context"
	"fmt"
	"go.etcd.io/etcd/clientv3"
	"time"
)

func main() {
	var (
		config  clientv3.Config
		client  *clientv3.Client
		err     error
		kv      clientv3.KV
		putResp *clientv3.PutResponse // 读写操作结果
	)

	config = clientv3.Config{
		Endpoints:   []string{"127.0.0.1:2379"}, //集群列表
		DialTimeout: 5 * time.Second,
	}

	// 建立一个客户端
	if client, err = clientv3.New(config); err != nil {
		fmt.Println(err)
		return
	}

	// 用于读写etcd的键值对
	kv = clientv3.NewKV(client)

	// context.TOD O() 上下文操作，这里代表什么都不做
	// ,clientv3.WithPrevKV() 如果需要获取覆盖的值需要设置
	if putResp,err = kv.Put(context.TODO(), "/cron/jobs/job1", "bey1",clientv3.WithPrevKV());err!=nil{
		fmt.Println(err)
		return
	}

	fmt.Println(putResp.Header.Revision)

	if putResp.PrevKv != nil{
		fmt.Println("被覆盖的值为：",string(putResp.PrevKv.Value))
	}
}

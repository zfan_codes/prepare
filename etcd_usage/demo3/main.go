package main

import (
	"context"
	"fmt"
	"go.etcd.io/etcd/clientv3"
	"time"
)

func main() {
	var (
		config  clientv3.Config
		client  *clientv3.Client
		err     error
		kv      clientv3.KV
		getResp *clientv3.GetResponse
	)

	config = clientv3.Config{
		Endpoints:   []string{"127.0.0.1:2379"}, //集群列表
		DialTimeout: 5 * time.Second,
	}

	// 建立一个客户端
	if client, err = clientv3.New(config); err != nil {
		fmt.Println(err)
		return
	}

	// 用于读写etcd的键值对
	kv = clientv3.NewKV(client)

	if getResp,err = kv.Get(context.TODO(),"/cron/jobs/job1");err!=nil{
		fmt.Println(err)
		return
	}else{
		// [key:"/cron/jobs/job1" create_revision:3 mod_revision:11 version:5 value:"bey1" ]
		// 创建的版本是3
		// 最新修改的版本是11
		// 一共修改了5次
		// 当前的值为 bey1
		fmt.Println(getResp.Kvs)
	}
}

package main

import (
	"context"
	"fmt"
	"go.etcd.io/etcd/clientv3"
	"go.etcd.io/etcd/mvcc/mvccpb"
	"time"
)

// 监听key的变化
func main() {
	var (
		config             clientv3.Config
		client             *clientv3.Client
		err                error
		kv                 clientv3.KV
		getResp            *clientv3.GetResponse
		watchStartRevision int64
		watcher            clientv3.Watcher
		watchRespChan      <-chan clientv3.WatchResponse
		watchResp          clientv3.WatchResponse
		event              *clientv3.Event
	)

	// 客户端配置
	config = clientv3.Config{
		Endpoints:   []string{"127.0.0.1:2379"}, // 连接客户端
		DialTimeout: 5 * time.Second,            // 设置超时时间
	}

	// 建立连接
	if client, err = clientv3.New(config); err != nil {
		fmt.Println(err)
		return
	}
	kv = clientv3.NewKV(client)

	go func() {
		for {
			kv.Put(context.TODO(), "/cron/jobs/job7", "I am job7")
			time.Sleep(2 * time.Second)
			kv.Delete(context.TODO(), "/cron/jobs/job7")
			time.Sleep(2 * time.Second)
		}
	}()

	// 先获取到当前的值，然后再监听后续的变化
	if getResp, err = kv.Get(context.TODO(), "cron/jobs/job7"); err != nil {
		fmt.Println(err)
		return
	}

	// 确定当前key是有值的
	if len(getResp.Kvs) != 0 {
		fmt.Println("当前值", string(getResp.Kvs[0].Value))
	}

	// 监听后续变化,从当前值+1开始监听
	watchStartRevision = getResp.Header.Revision + 1

	// 创建一个watcher
	watcher = clientv3.NewWatcher(client)

	// 启动监听
	fmt.Println("从该版本向后监听:", watchStartRevision)

	watchRespChan = watcher.Watch(context.TODO(), "cron/jobs/job7", clientv3.WithRev(watchStartRevision))

	// 处理kv的变化事件
	for watchResp = range watchRespChan {
		for _, event = range watchResp.Events {
			switch event.Type {
			case mvccpb.PUT:
				fmt.Println("修改为：", string(event.Kv.Value), "rev:", event.Kv.CreateRevision, event.Kv.ModRevision)
			case mvccpb.DELETE:
				fmt.Println("删除,rev:", event.Kv.ModRevision)

			}
		}
	}

}
